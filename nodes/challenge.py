#!/usr/bin/env python

from geometry_msgs.msg import Twist
import rospy
from gazebo_msgs.srv import GetModelState
import math
import sys
from std_msgs.msg import String

class Vector3:
    def __init__(self, x = 0, y = 0, z = 0):
        self.x = x
        self.y = y
        self.z = z

class Quaternion:
    def __init__(self, x = 0, y = 0, z = 0, w = 0):
        self.x = x
        self.y = y
        self.z = z
        self.w = w

class EulerAngles:

    def __init__(self, roll = 0, pitch = 0, yaw = 0):
        self.roll = roll
        self.pitch = pitch
        self.yaw = yaw

    def quaternion_to_euler_angles(self, q):
        # https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
        # roll (x-axis rotation)
        sinr_cosp = 2 * (q.w * q.x + q.y * q.z)
        cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y)
        self.roll = math.atan2(sinr_cosp, cosr_cosp)

        # pitch (y-axis rotation)
        sinp = 2 * (q.w * q.y - q.z * q.x)
        if abs(sinp) >= 1:
            self.pitch = math.copysign(math.pi/2, sinp)
        else:
            self.pitch = math.asin(sinp)

        # yaw (z-axis rotation)
        siny_cosp = 2 * (q.w * q.z + q.x * q.y)
        cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z)
        self.yaw = math.atan2(siny_cosp, cosy_cosp)
        

def call_get_model_state(model_name):
    rospy.wait_for_service('/gazebo/get_model_state')
    try:
        get_model_state = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
        response = get_model_state(model_name, None)
        return response
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

def get_target_direction(initial_point, end_point):
    vector = Vector3(end_point.x - initial_point.x, end_point.y - initial_point.y, end_point.z - initial_point.z )
    return(vector)

def euler_angles_to_vector(e):
    vector = Vector3(math.cos(e.yaw)*math.cos(e.pitch), math.sin(e.yaw)*math.cos(e.pitch), math.sin(e.pitch))
    return vector

def get_angle(first_vector, second_vector):
    cos_of_angle = dot(first_vector, second_vector)/(len_of_vector(first_vector)*len_of_vector(second_vector))
    angle = math.acos(cos_of_angle)
    cross_res = cross(first_vector, second_vector)
    return math.copysign(angle,cross_res.z)


def cross(a, b):
    c = Vector3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x)
    return c

def len_of_vector(vector):
    len = math.sqrt(vector.x**2 + vector.y**2 + vector.z**2)
    return len

def dot(a, b):
    result = (a.x*b.x + a.y*b.y + a.z*b.z)
    return result

def rotate(p_angle, i_angle, d_angle):
    cmd_msg = Twist()

    kp = 1
    ki = 0
    kd = 0.5

    rotation = kp*p_angle + ki*i_angle + kd*d_angle
    cmd_msg.angular.z = -rotation
    cmd_msg.linear.x = abs(abs(p_angle) - math.pi)/(1.5*math.pi)
    return cmd_msg

def slow_down(target_vector):
    x = len_of_vector(target_vector)
    speed = -(20**(-x)) + 1
    return speed

if __name__ == '__main__':
    checkpoint_location = Vector3(0,0,0)
    temp_angle = 0
    i_angle = 0
    msg = Twist()

    rospy.init_node("challenge_demo")
    pub = rospy.Publisher("/cmd_vel/", Twist, queue_size=10)
 
    
    while ~rospy.is_shutdown():
        ms = call_get_model_state("robot")
        q = Quaternion(ms.pose.orientation.x, ms.pose.orientation.y, ms.pose.orientation.z, ms.pose.orientation.w)
        model_pose = Vector3(ms.pose.position.x, ms.pose.position.y, ms.pose.position.z)

        e = EulerAngles()
        e.quaternion_to_euler_angles(q)

        target_vector = get_target_direction(model_pose, checkpoint_location)
        target_vector.z = 0 # We only care about vector at x-y direction

        temp_e = EulerAngles(0,0,e.yaw)  # We only care about angle at Yaw
        current_vector = euler_angles_to_vector(temp_e)

        angle = get_angle(target_vector, current_vector)
        i_angle = angle + i_angle
        msg = rotate(angle, i_angle, angle-temp_angle)
        speed_rate = slow_down(target_vector)
        msg.linear.x = msg.linear.x*slow_down(target_vector)
        if len_of_vector(target_vector) < 0.2:
            msg.angular.z = 0
            msg.linear.x = 0
        pub.publish(msg)
        temp_angle = angle
